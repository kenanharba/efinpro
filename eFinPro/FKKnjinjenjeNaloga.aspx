﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Glavna.Master" AutoEventWireup="true" CodeBehind="FKKnjinjenjeNaloga.aspx.cs" Inherits="eFinProWeb.FKKnjinjenjeNaloga" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblSessionUsername" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblSessionFirma" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblSessionGodina" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblSessionOP" runat="server" Text="" Visible="false"></asp:Label>
    <br />
    <br />
    <asp:Label runat="server" Text="Vrsta naloga za knjiženje"></asp:Label>
    <asp:DropDownList ID="cbxVrNalKnj" runat="server" DataSourceID="SqlDataSource" DataTextField="OpisNaloga" DataValueField="IDVrNaloga" AutoPostBack="True" OnTextChanged="cbxVrNalKnj_TextChanged">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDVrNaloga], [OpisNaloga] FROM [ZFVrstaNaloga] WHERE ([IDFirma] = @IDFirma)">
        <SelectParameters>
            <asp:SessionParameter Name="IDFirma" SessionField="Firma" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Label runat="server" Text="Br. naloga"></asp:Label>
    <asp:TextBox ID="txtBrNaloga" runat="server" Text="0" Width="30px" Style="text-align: center;"></asp:TextBox>
    <asp:Label runat="server" Text="Datum naloga"></asp:Label>
    <asp:TextBox ID="txtDatum" runat="server" Width="75px"></asp:TextBox>
    <asp:Button ID="Button2" runat="server" Text="..." OnClick="Button2_Click" />
    <asp:Calendar ID="CalPD" runat="server" Style="margin-top: 0px; margin-left: 200px;" OnSelectionChanged="CalPD_SelectionChanged" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px" SelectedDate="1.1.0001" VisibleDate="1.1.0001">
        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
        <NextPrevStyle VerticalAlign="Bottom" />
        <OtherMonthDayStyle ForeColor="#808080" />
        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
        <SelectorStyle BackColor="#CCCCCC" />
        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
        <WeekendDayStyle BackColor="#FFFFCC" />
    </asp:Calendar>
    <asp:Label runat="server" Text="Org. Jed."></asp:Label>
    <asp:DropDownList ID="cbxOrgJed" runat="server" DataSourceID="SqlDataSource3" DataTextField="OrgJed" DataValueField="IDOrg">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDOrg], [OrgJed] FROM [ZFOrgJed] WHERE ([IDFirma] = @IDFirma)">
        <SelectParameters>
            <asp:SessionParameter Name="IDFirma" SessionField="Firma" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Button ID="btnNoviNalog" runat="server" Text="Kreiraj Nalog" OnClick="btnNoviNalog_Click" />
    <p>
        <asp:Label ID="lblPorika" runat="server" Font-Size="20px" Font-Bold="true" ForeColor="DarkRed" Text="" Visible="false"></asp:Label>
    </p>
    <br />
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Konto"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="cbxKOnto" runat="server" DataSourceID="SqlDataSource4" DataTextField="konto" DataValueField="Konto" OnTextChanged="cbxKOnto_TextChanged" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDKonto], [konto] FROM [FinKontniPlan] WHERE ([IDFirma] = @IDFirma) order by konto">
                        <SelectParameters>
                            <asp:SessionParameter Name="IDFirma" SessionField="Firma" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>
                    <asp:Button ID="btnKonto" runat="server" Text="..." OnClick="btnKonto_Click" />
                </td>
                <td>
                    <asp:TextBox ID="txtDatumK" runat="server" Width="75px"></asp:TextBox>
                    <asp:Button ID="Button3" runat="server" Text="..." OnClick="Button3_Click" />
                    <asp:Calendar ID="CalDK" runat="server" Style="margin-top: 0px; margin-left: 200px;" OnSelectionChanged="CalDK_SelectionChanged" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px" SelectedDate="1.1.0001" VisibleDate="1.1.0001">
                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                        <NextPrevStyle VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="#808080" />
                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                        <SelectorStyle BackColor="#CCCCCC" />
                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <WeekendDayStyle BackColor="#FFFFCC" />
                    </asp:Calendar>
                </td>
            </tr>
        </table>
    </div>
    <br />
</asp:Content>
