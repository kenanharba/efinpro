﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;

namespace eFinProWeb
{
    public partial class MaticniPodatciOFirmi : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            if (cbxMaticaKcerka.Text == "DA")
            {
                cbxMatica.Visible = false;
                lblNazivKcerkeFirme.Visible = false;
            }
            else
            {
                cbxMatica.Visible = true;
                lblNazivKcerkeFirme.Visible = true;
            }
        }

        protected void Spasi_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            var KorID = Session["KorID"];
            SqlDataAdapter cmd = new SqlDataAdapter("select brlicenci from tlbKorisnik where korisnikID=" + KorID, Conn);
            DataTable ds = new DataTable();
            cmd.Fill(ds);
            SqlCommand cmd1 = new SqlCommand("select COUNT(*) from tlbMatPodatci INNER JOIN tlbPeriod ON tlbMatPodatci.IDFirma = tlbPeriod.IDFirma WHERE tlbMatPodatci.KorisnikID=" + KorID, Conn);
            int count = Convert.ToInt32(cmd1.ExecuteScalar());
            int a = Convert.ToInt32(ds.Rows[0][0]);
            if (count < a)
            {
                if ((txtIDBroj.Text == "") || (string.IsNullOrEmpty(txtIDBroj.Text)))
                {
                    Response.Write("<script>alert('ID Broj je obavezan podatak');</script>");
                    txtIDBroj.Focus();
                }
                else
                {
                    SqlCommand cmdS = new SqlCommand("insert into tlbMatPodatci values('" + txtPuniNazivDrustva.Text + "', '" + txtSkraceniNazivDrustva.Text + "', '" + txtAdresa.Text + "', '" + Convert.ToDouble(txtBroj.Text) + "', '" + Convert.ToDouble(txtPostanskiBroj.Text) + "', '" + txtSjediste.Text + "', '" + cbxIDOpcina.Text + "', '" + cbxRegija.Text + "', '" + txtIDBroj.Text + "', '" + txtPDVBroj.Text + "', '" + txtSifraDjelatnosti.Text + "', '" + txtSifraDjelatnostiNova.Text + "', '" + txtDjelatnost.Text + "', '" + txtTel.Text + "', '" + txtFax.Text + "', '" + txtEmail.Text + "', '" + cbxVrstaOrganizacije.Text + "', '" + cbxMaticaKcerka.Text + "', '" + cbxMatica.Text + "', '1', '" + txtBrPoslovnihJedinica.Text + "', '" + txtBankaNaziv1.Text + "', '0', '" + txtBankaNaziv2.Text + "', '0', '" + txtBankaNaziv3.Text + "', '0', '" + txtBankaNaziv4.Text + "', '0', '" + txtBankaNaziv5.Text + "', '0')", Conn);
                    cmdS.ExecuteNonQuery();
                    Conn.Close();
                }               
            }
            else
            {
                Response.Write("<script>alert('Broj plačenih licenci iznosi " + a + " broj aktivnih Perioda je " + count + ".');</script>");
            }
        }

        protected void btnVlasnici_Click(object sender, EventArgs e)
        {
            Response.Redirect("Vlasnici.aspx");
        }

        protected void btnRacunovodza_Click(object sender, EventArgs e)
        {
            Response.Redirect("Racunovodza.aspx");
        }

        protected void btnOdgOsoba_Click(object sender, EventArgs e)
        {
            Response.Redirect("OdgovornaOsoba.aspx");
        }

        protected void txtIDBroj_TextChanged(object sender, EventArgs e)
        {
            Conn.Open();
            SqlCommand cmd = new SqlCommand("select count(*) from tlbMatPodatci where idbroj='" + txtIDBroj.Text + "'", Conn);
            int count = Convert.ToInt32(cmd.ExecuteScalar());
            if (count == 0)
            {
                var tmpStr1 = txtIDBroj.Text;
                var tmpStr2 = tmpStr1.Substring(1, 12);
                Console.WriteLine(tmpStr2);
                txtPDVBroj.Text = tmpStr2;
                txtPDVBroj.Focus();
            }
            else
            {
                SqlDataAdapter cmd1 = new SqlDataAdapter("select * from tlbMatPodatci where idbroj='" + txtIDBroj.Text + "'", Conn);
                DataTable ds = new DataTable();
                cmd1.Fill(ds);
                txtPuniNazivDrustva.Text = ds.Rows[0][1].ToString();
                txtSkraceniNazivDrustva.Text = ds.Rows[0][2].ToString();
                txtAdresa.Text = ds.Rows[0][3].ToString();
                txtBroj.Text = ds.Rows[0][4].ToString();
                txtPostanskiBroj.Text = ds.Rows[0][5].ToString();
                txtSjediste.Text = ds.Rows[0][6].ToString();                
                //cbxIDOpcina.Text = ds.Rows[0][7].ToString();
                cbxRegija.Text = ds.Rows[0][8].ToString();
                txtPDVBroj.Text = ds.Rows[0][10].ToString();
                txtSifraDjelatnosti.Text = ds.Rows[0][11].ToString();
                txtSifraDjelatnostiNova.Text = ds.Rows[0][12].ToString();
                txtDjelatnost.Text = ds.Rows[0][13].ToString();
                txtTel.Text = ds.Rows[0][14].ToString();
                txtFax.Text = ds.Rows[0][15].ToString();
                txtEmail.Text = ds.Rows[0][16].ToString();
                cbxVrstaOrganizacije.Text = ds.Rows[0][17].ToString();
                cbxMaticaKcerka.Text = ds.Rows[0][18].ToString();
                cbxMatica.Text = ds.Rows[0][19].ToString();
                txtBrPoslovnihJedinica.Text = ds.Rows[0][20].ToString();
                txtBankaNaziv1.Text = ds.Rows[0][21].ToString();
                txtRacunBroj1.Text = ds.Rows[0][22].ToString();
                txtBankaNaziv2.Text = ds.Rows[0][23].ToString();
                txtRacunBroj2.Text = ds.Rows[0][24].ToString();
                txtBankaNaziv3.Text = ds.Rows[0][25].ToString();
                txtRacunBroj3.Text = ds.Rows[0][26].ToString();
                txtBankaNaziv4.Text = ds.Rows[0][27].ToString();
                txtRacunBroj4.Text = ds.Rows[0][28].ToString();
                txtBankaNaziv5.Text = ds.Rows[0][29].ToString();
                txtRacunBroj5.Text = ds.Rows[0][30].ToString();
                txtPuniNazivDrustva.Focus();
            }
        }

        protected void txtBankaNaziv1_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtBankaNaziv1.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtBankaNaziv1.Text = upper1;
            txtRacunBroj1.Focus();
        }

        protected void txtBankaNaziv2_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtBankaNaziv2.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtBankaNaziv2.Text = upper1;
            txtRacunBroj2.Focus();
        }

        protected void txtBankaNaziv3_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtBankaNaziv3.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtBankaNaziv3.Text = upper1;
            txtRacunBroj3.Focus();
        }

        protected void txtBankaNaziv4_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtBankaNaziv4.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtBankaNaziv4.Text = upper1;
            txtRacunBroj4.Focus();
        }

        protected void txtBankaNaziv5_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtBankaNaziv5.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtBankaNaziv5.Text = upper1;
            txtRacunBroj5.Focus();
        }

        protected void txtPuniNazivDrustva_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtPuniNazivDrustva.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtPuniNazivDrustva.Text = upper1;
            txtSkraceniNazivDrustva.Text = txtPuniNazivDrustva.Text;
            txtSkraceniNazivDrustva.Focus();
        }

        protected void txtSkraceniNazivDrustva_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtSkraceniNazivDrustva.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtSkraceniNazivDrustva.Text = upper1;
        }

        protected void txtAdresa_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtAdresa.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtAdresa.Text = upper1;
            txtBroj.Focus();
        }

        protected void txtSjediste_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtSjediste.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtSjediste.Text = upper1;
            cbxRegija.Focus();
        }

        protected void txtDjelatnost_TextChanged(object sender, EventArgs e)
        {
            string value1 = txtDjelatnost.Text;
            string upper1 = value1.ToUpper();
            Console.WriteLine(upper1);
            txtDjelatnost.Text = upper1;
            txtTel.Focus();
        }
    }
}