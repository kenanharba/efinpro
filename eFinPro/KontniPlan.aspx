﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Glavna.Master" AutoEventWireup="true" CodeBehind="KontniPlan.aspx.cs" Inherits="eFinProWeb.KontniPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .lijevo {
            margin-left: 100px;
        }
    </style>
    <asp:Label ID="lblSessionUsername" runat="server" Text="" Visible="false"></asp:Label>
    <br />
    <br />
    <asp:Label Font-Size="26px" ForeColor="black" Width="100%" Style="text-align: center; font-weight: bold" runat="server" Text="UNOS KONTA"></asp:Label>
    <br />

    <div class="lijevo">
        <p>
            <tr>
                <td>
                    <asp:Label Text="Analitički Konto" runat="server" Width="150px"></asp:Label>
                    <asp:TextBox ID="txtAnaKonto" runat="server" Style="text-align: right; margin-left: 50px" Width="50px" OnTextChanged="txtAnaKonto_TextChanged" AutoPostBack="true"></asp:TextBox>
                </td>
                <td>
                    <asp:Label Text="Konto je sintetički" runat="server" Width="150px"></asp:Label>
                    <asp:CheckBox ID="chbSintetika" runat="server" /></td>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label Text="Opis Konta" runat="server" Width="150px"></asp:Label>
                    <asp:TextBox ID="txtOpis" runat="server" Style="text-align: left; margin-left: 50px" Width="300px"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label Text="Konto je SubAnalitički" runat="server" Width="150px"></asp:Label>
                    <asp:CheckBox ID="chbSubAnalitika" runat="server" /></td>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label Text="Prenos po Org. Jed" runat="server" Width="150px"></asp:Label>
                    <asp:CheckBox ID="chbPrenosOrgJed" runat="server" /></td>
                </td>
            </tr>
        </p>
    </div>
        <div class="container">
            <asp:Button ID="btnSpasi" runat="server" Text="Spremi" Width="180px" Height="45px" OnClick="btnSpasi_Click"/>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <asp:Button ID="btnIspis" runat="server" Text="Ispis kontnog plana" Width="180px" Height="45px"/>
       </div>

</asp:Content>
