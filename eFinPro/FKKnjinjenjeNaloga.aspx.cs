﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;

namespace eFinProWeb
{
    public partial class FKKnjinjenjeNaloga : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            if (!IsPostBack)
            {
                lblSessionUsername.Text += Session["username"];
                lblSessionFirma.Text += Session["firma"];
                lblSessionGodina.Text += Session["Godina"];
                if ((lblSessionUsername.Text == "") || (string.IsNullOrEmpty(lblSessionUsername.Text)))
                {
                    Response.Write("<script>alert('Sesija istekla');</script>");
                    Response.Redirect("login.aspx");
                }
                else
                {
                    CalPD.Visible = false;
                    CalDK.Visible = false;
                    lblSessionUsername.Text += Session["username"];
                    cbxVrNalKnj.Focus();
                }
            }
        }

        //private void populateGrigView()
        //{
        //    Conn.Open();
        //    var Firma = Session["firma"];
        //    var Godina = Session["Godina"];
        //    var KorID = Session["Username"];
        //    SqlDataAdapter cmd = new SqlDataAdapter("SELECT * FROM FinGKS WHERE idfirma=" + Firma + " and idperiod=" + Godina + "  ORDER BY id", Conn);
        //    DataTable ss = new DataTable();
        //    cmd.Fill(ss);

        //    if (ss.Rows.Count > 0)
        //    {
        //        gvPregledKnjizenja.DataSource = ss;
        //        gvPregledKnjizenja.DataBind();
        //    }
        //    else
        //    {
        //        ss.Rows.Add(ss.NewRow());
        //        gvPregledKnjizenja.DataSource = ss;
        //        gvPregledKnjizenja.DataBind();
        //        gvPregledKnjizenja.Rows[0].Cells.Clear();
        //        gvPregledKnjizenja.Rows[0].Cells.Add(new TableCell());
        //        gvPregledKnjizenja.Rows[0].Cells[0].ColumnSpan = ss.Columns.Count;
        //        gvPregledKnjizenja.Rows[0].Cells[0].Text = "Podatci nisu obrađeni ... !!!";
        //        gvPregledKnjizenja.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
        //    }

        //}

        protected void cbxVrNalKnj_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (CalPD.Visible == false)
            {
                CalPD.Visible = true;
            }
            else
            {
                CalPD.Visible = false;
            }
        }

        protected void CalPD_SelectionChanged(object sender, EventArgs e)
        {
            txtDatum.Text = CalPD.SelectedDate.ToShortDateString();
            CalPD.Visible = false;
        }

        protected void btnNoviNalog_Click(object sender, EventArgs e)
        {
            Conn.Open();
            lblPorika.Visible = false;
            var firma = Session["firma"];
            var godina = Session["Godina"];

            SqlDataAdapter cmd = new SqlDataAdapter("select BrNalogaZadnji From ZFVrstaNaloga where idfirma=" + firma + " and IDPeriod=" + godina + " and IDVrNaloga=" + cbxVrNalKnj.DataValueField , Conn);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (Convert.ToInt32(txtBrNaloga.Text) == 0)
            {
                txtBrNaloga.Text = Convert.ToInt32(Convert.ToInt32(dt.Rows[0][0])+1).ToString();
                lblPorika.Visible = true;
                lblPorika.Text = "Nalog kreiran i spreman za rad";
            }
            else
            {
                lblPorika.Visible = true;
                lblPorika.Text = "Broj naloga je izmjenjen. Razlog broj se već koristi";
                txtBrNaloga.Text = Convert.ToInt32(Convert.ToInt32(dt.Rows[0][0]) + 1).ToString();
            }
            SqlDataAdapter cmdP = new SqlDataAdapter("", Conn);
            double nalog = 1;
            SqlCommand cmdS = new SqlCommand("insert into fingkg (IDNalog, IDVrstaNaloga, IDOrg, IDFirma, IDPeriod, BrNaloga, Datum) values " +
                "( " + nalog + ")", Conn);
            cmdS.ExecuteNonQuery();
        }

        protected void btnKonto_Click(object sender, EventArgs e)
        {
            Response.Redirect("KontniPlan.aspx");
        }

        protected void CalDK_SelectionChanged(object sender, EventArgs e)
        {
            txtDatumK.Text = CalDK.SelectedDate.ToShortDateString();
            CalDK.Visible = false;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (CalDK.Visible == false)
            {
                CalDK.Visible = true;
            }
            else
            {
                CalDK.Visible = false;
            }
        }

        protected void cbxKOnto_TextChanged(object sender, EventArgs e)
        {
            Conn.Open();
            var firma = Session["firma"];
            var KorID = Session["korid"];
            SqlDataAdapter cmd = new SqlDataAdapter("select * from FinKontniPlan where konto = '" + cbxKOnto.SelectedValue + " and idkorisnik=" + KorID, Conn);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

        }
    }
}