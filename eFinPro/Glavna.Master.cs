﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace eFinProWeb
{
    public partial class Glavna : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Url.ToString().ToLower().IndexOf("default.aspx") > 1) || (Request.Url.ToString().ToLower().IndexOf(".aspx") == -1))
            {
                pnMeni.Visible = false;
            }
            else
            {
                pnMeni.Visible = true;
            }

            if ((Request.Url.ToString().ToLower().IndexOf("signin.aspx") > 1) || (Request.Url.ToString().ToLower().IndexOf(".aspx") == -1))
            {
                pnMeni.Visible = false;
            }
            else
            {
                pnMeni.Visible = true;
            }

            if ((Request.Url.ToString().ToLower().IndexOf("Login.aspx") > 1) || (Request.Url.ToString().ToLower().IndexOf(".aspx") == -1))
            {
                pnMeni.Visible = false;
            }
            else
            {
                pnMeni.Visible = true;
            }

            if ((Request.Url.ToString().ToLower().IndexOf("IzborFirme.aspx") > 1) || (Request.Url.ToString().ToLower().IndexOf(".aspx") == -1))
            {
                pnMeni.Visible = false;
            }
            else
            {
                pnMeni.Visible = true;
            }

            if ((Request.Url.ToString().ToLower().IndexOf("PregledPerioda.aspx") > 1) || (Request.Url.ToString().ToLower().IndexOf(".aspx") == -1))
            {
                pnMeni.Visible = false;
            }
            else
            {
                pnMeni.Visible = true;
            }

            if ((Request.Url.ToString().ToLower().IndexOf("MaticniPodatciOFirmi.aspx") > 1) || (Request.Url.ToString().ToLower().IndexOf(".aspx") == -1))
            {
                pnMeni.Visible = false;
            }
            else
            {
                pnMeni.Visible = true;
            }
        }
    } 
}