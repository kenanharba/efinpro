﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace eFinProWeb
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
        private string korid;

        protected void Page_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
        }

        protected void Spasi_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            SqlCommand cmd = new SqlCommand("select COUNT(1) from ZFKorisnici WHERE NazivKorisnika=@UserName AND Password=@Password", Conn);
            cmd.Parameters.AddWithValue("@UserName", txtUserName.Text.Trim());
            cmd.Parameters.AddWithValue("@Password", txtPassWord.Text.Trim());
            
            int count = Convert.ToInt32(cmd.ExecuteScalar());
            if (count == 1)
            {
                string a = txtUserName.Text.Trim();
                string b = txtPassWord.Text.Trim();
                lblErrMgb.Visible = true;
                SqlDataAdapter cmd1 = new SqlDataAdapter("select IDKorisnik from ZFKorisnici WHERE NazivKorisnika like '" + a + "' AND Password like '" + b + "'", Conn);
                DataTable dt = new DataTable();
                cmd1.Fill(dt);

                Session["username"] = txtUserName.Text.Trim();
                Session["korid"] = dt.Rows[0][0];
                Response.Redirect("izborfirme.aspx");
            }
            else {
                lblErrMgb.Visible = true;
                lblErrMgb.Text = "Korisnik ili Password nije ispravan probaj ponovo";
                txtPassWord.Focus();
            }
        }

        protected void btnRegistar_Click(object sender, EventArgs e)
        {
            Response.Redirect("SignIn.aspx");
        }
    }
}