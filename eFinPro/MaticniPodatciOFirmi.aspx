﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Glavna.Master" AutoEventWireup="true" CodeBehind="MaticniPodatciOFirmi.aspx.cs" Inherits="eFinProWeb.MaticniPodatciOFirmi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style type="text/css">
        .auto-style5 {
            overflow: hidden;
        }
    </style>
   <br />
    <br />
    <div style="margin-left: 50px" class="auto-style5">
        <p>
            <tr>
                <td>
                    <asp:Label Text="Puni naziv društva" runat="server" Width="150px"></asp:Label>
                    <asp:TextBox ID="txtPuniNazivDrustva" runat="server" Style="text-align: left; margin-left: 50px" Width="410px" OnTextChanged="txtPuniNazivDrustva_TextChanged" AutoPostBack="true"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label Text="Skraćeni Naziv Društva" runat="server" Width="190px"></asp:Label>
                    <asp:TextBox ID="txtSkraceniNazivDrustva" runat="server" Style="margin-left: 10px" Width="410px" OnTextChanged="txtSkraceniNazivDrustva_TextChanged" AutoPostBack="true"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label Text="Adresa Ulica" runat="server" Width="190px"></asp:Label>
                    <asp:TextBox ID="txtAdresa" runat="server" Style="margin-left: 10px; margin-left: 10px" Width="315px" OnTextChanged="txtAdresa_TextChanged" AutoPostBack="true"></asp:TextBox>
                    &nbsp;
                <asp:Label Text="Broj" runat="server" Width="40px"></asp:Label>
                    <asp:TextBox ID="txtBroj" runat="server" Style="margin-left: 0px; margin-right: 0px" Width="40px"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label Text="Poštanski broj" runat="server" Width="190px"></asp:Label>
                    <asp:TextBox ID="txtPostanskiBroj" runat="server" Style="margin-left: 10px" Width="70px"></asp:TextBox>
                    &nbsp;
                <asp:Label Text="Sjedište" runat="server" Width="60px"></asp:Label>
                    <asp:TextBox ID="txtSjediste" runat="server" Style="margin-left: 10px; margin-right: 10px" Width="160px" OnTextChanged="txtSjediste_TextChanged" AutoPostBack="true"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Regija / Kanton" Width="190px"></asp:Label>
                    <asp:DropDownList ID="cbxRegija" runat="server" DataSourceID="SqlDataSource1" DataTextField="NazivRegije" DataValueField="IDRegija" Style="margin-left: 10px; margin-right: 10px" Width="150px" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDRegija], [NazivRegije] FROM [tlbRegija] ORDER BY [IDRegija]"></asp:SqlDataSource>
                    <asp:Label runat="server" Text="Općina" Width="50px"></asp:Label>
                    <asp:DropDownList ID="cbxIDOpcina" runat="server" DataSourceID="SqlDataSource2" DataTextField="NazivOpcina" DataValueField="IDOpcina" Style="margin-left: 10px; margin-right: 10px" Width="200px"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDOpcina], [NazivOpcina], [SOpcina] FROM [tlbOpcine] WHERE ([IDRegija] = @IDRegija)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cbxRegija" Name="IDRegija" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label runat="server" Text="ID BROJ" Width="190px"></asp:Label>
                    <asp:TextBox ID="txtIDBroj" class="Idbr" runat="server" Style="margin-left: 10px" AutoPostBack="true" OnTextChanged="txtIDBroj_TextChanged"></asp:TextBox>
                    &nbsp;
                <asp:Label runat="server" Text="PDV BROJ" Width="80px"></asp:Label>
                    <asp:TextBox ID="txtPDVBroj" class="IdPdv" runat="server" Style="margin-left: 10px"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Šifra Djelatnosti" Width="190px"></asp:Label>
                    <asp:TextBox ID="txtSifraDjelatnosti" class="SD" runat="server" Style="margin-left: 10px; margin-right: 10px;" Width="50px"></asp:TextBox>
                    <asp:Label runat="server" Text="Šifra Djelatnosti Nova" Width="170px"></asp:Label>
                    <asp:TextBox ID="txtSifraDjelatnostiNova" class="SDN" runat="server" Style="margin-left: 10px; margin-right: 10px" Width="40px"></asp:TextBox>
                    <asp:Label runat="server" Text="Djelatnost" Width="80px"></asp:Label>
                    <asp:TextBox ID="txtDjelatnost" runat="server" Style="margin-left: 10px" Width="410px" OnTextChanged="txtDjelatnost_TextChanged" AutoPostBack="true"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Telefon" Width="190px"></asp:Label>
                    <asp:TextBox ID="txtTel" runat="server" Style="margin-left: 10px; margin-right: 10px;"></asp:TextBox>
                    <asp:Label runat="server" Text="Fax." Width="20px"></asp:Label>
                    <asp:TextBox ID="txtFax" runat="server" Style="margin-left: 10px; margin-right: 10px;"></asp:TextBox>
                    <asp:Label runat="server" Text="e-mail" Width="40px"></asp:Label>
                    <asp:TextBox ID="txtEmail" runat="server" Style="margin-left: 10px; margin-right: 10px;" Width="410px"></asp:TextBox>
                </td>
            </tr>
        </p>
        <p>
            <tr>
                <td>
                    <asp:Label runat="server" Text="Vrsta Organizacije" Width="190px" Style="margin-left: 0px; margin-right: 10px;"></asp:Label>
                    <asp:DropDownList ID="cbxVrstaOrganizacije" runat="server" DataSourceID="SqlDataSource3" DataTextField="VrstaOrganizacije" DataValueField="VrstaOrganizacijeID" Width="135px"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT * FROM [tlbVrOrgFirme]"></asp:SqlDataSource>
                    <asp:Label runat="server" Text="Društvo je Matica" Width="145px" Style="margin-left: 10px; margin-right: 10px;"></asp:Label>
                    <asp:DropDownList ID="cbxMaticaKcerka" runat="server" AutoPostBack="True">
                        <asp:ListItem Text="DA"></asp:ListItem>
                        <asp:ListItem Text="Kčerka firam >=50% kapitala"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lblNazivKcerkeFirme" runat="server" Text=" Naziv kčerke firme" Width="145px" Style="margin-left: 10px; margin-right: 10px;"></asp:Label>
                    <asp:DropDownList ID="cbxMatica" runat="server" DataSourceID="SqlDataSource4" DataTextField="PuniNazivDrustva" DataValueField="IDFirma" Width="225px">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDFirma], [PuniNazivDrustva] FROM [tlbMatPodatci]"></asp:SqlDataSource>
                    <asp:Label runat="server" Text="BrPoslovnihJedinica" Width="190px"></asp:Label>
                    <asp:TextBox ID="txtBrPoslovnihJedinica" Text="1" runat="server" Width="44px"></asp:TextBox>
                </td>
            </tr>
        </p>

        <p>
            <br />
            Naziv Banke&nbsp;&nbsp;<asp:TextBox ID="txtBankaNaziv1" runat="server" OnTextChanged="txtBankaNaziv1_TextChanged" AutoPostBack="true"></asp:TextBox>
            Transakciski Račun Glavni&nbsp;&nbsp;<asp:TextBox ID="txtRacunBroj1" runat="server" Class="TR"></asp:TextBox>
            <br />
            Naziv Banke&nbsp;&nbsp;<asp:TextBox ID="txtBankaNaziv2" runat="server" OnTextChanged="txtBankaNaziv2_TextChanged" AutoPostBack="true"></asp:TextBox>
            Transakciski Račun &nbsp;&nbsp;<asp:TextBox ID="txtRacunBroj2" runat="server"></asp:TextBox>
            <br />
            Naziv Banke&nbsp;&nbsp;<asp:TextBox ID="txtBankaNaziv3" runat="server" OnTextChanged="txtBankaNaziv3_TextChanged" AutoPostBack="true"></asp:TextBox>
            Transakciski Račun &nbsp;&nbsp;<asp:TextBox ID="txtRacunBroj3" runat="server"></asp:TextBox>
            <br />
            Naziv Banke&nbsp;&nbsp;<asp:TextBox ID="txtBankaNaziv4" runat="server" OnTextChanged="txtBankaNaziv4_TextChanged" AutoPostBack="true"></asp:TextBox>
            Transakciski Račun &nbsp;&nbsp;<asp:TextBox ID="txtRacunBroj4" runat="server"></asp:TextBox>
            <br />
            Naziv Banke&nbsp;&nbsp;<asp:TextBox ID="txtBankaNaziv5" runat="server" OnTextChanged="txtBankaNaziv5_TextChanged" AutoPostBack="true"></asp:TextBox>
            Transakciski Račun &nbsp;&nbsp;<asp:TextBox ID="txtRacunBroj5" runat="server"></asp:TextBox>
        </p>
        <br />
        <script src="Scripts/jquery.js" type="text/javascript"></script>
        <script src="Scripts/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $('.Idbr').mask('9999999999999');
            $('.IdPdv').mask('999999999999');
            $('.SD').mask('99999');
            $('.SDN').mask('9999');
            $('.TR').mask('9999999999999999');
        </script>

        <asp:Button ID="spasi" runat="server" OnClick="Spasi_Click" Text="Spasi" Width="180px" Height="45px" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnVlasnici" runat="server" OnClick="btnVlasnici_Click" Text="Vlasnici" Width="180px" Height="45px" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnRacunovodza" runat="server" OnClick="btnRacunovodza_Click" Text="Računovođa" Width="180px" Height="45px" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnOdgOsoba" runat="server" OnClick="btnOdgOsoba_Click" Text="Odgovorna Osoba" Width="180px" Height="45px" />
    </div>
    <br />
</asp:Content>
