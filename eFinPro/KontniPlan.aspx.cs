﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;

namespace eFinProWeb
{
    public partial class KontniPlan : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            if (!IsPostBack)
            {
                lblSessionUsername.Text += Session["username"];
                if ((lblSessionUsername.Text == "") || (string.IsNullOrEmpty(lblSessionUsername.Text)))
                {
                    Response.Write("<script>alert('Sesija istekla');</script>");
                    Response.Redirect("login.aspx");
                }
                else
                {                  
                    lblSessionUsername.Text += Session["username"];
                    txtAnaKonto.Focus();
                }
            }
        }

        protected void btnSpasi_Click(object sender, EventArgs e)
        {
            Conn.Open();
            var Firma = Session["firma"];
            var Godina = Session["Godina"];
            var KorID = Session["korid"];
            var tmpStr1 = txtAnaKonto.Text;
            var tmpStr2 = tmpStr1.Substring(0, 3);
            Console.WriteLine(tmpStr2);
            string sd = tmpStr2;
            var tmpStr21 = tmpStr1.Substring(0, 2);
            Console.WriteLine(tmpStr21);
            string k2 = tmpStr21;
            var tmpStr22 = tmpStr1.Substring(0, 1);
            Console.WriteLine(tmpStr22);
            string k = tmpStr22;

            SqlDataAdapter cmd = new SqlDataAdapter("select * from FinKontniPlan where idfirma=" + Firma + " and konto like '" + txtAnaKonto.Text + "'", Conn);
            DataTable ss = new DataTable();
            cmd.Fill(ss);

            if (ss.Rows.Count == 0)
            {
                SqlCommand cmd1 = new SqlCommand("insert into FinKontniPlan ( IDFirma, Konto, NazivKonta, SubAnalitika, PrenosPoOrgJed, Sintetika, IDKorisnik, SinDio, klasa, klasa2 ) values " +
                "( " + Firma + ", '" + txtAnaKonto.Text + "', '" + txtOpis.Text + "', @sa, @po, @s, " + KorID + ", '" + sd + "', '" + k + "', '" + k2 + "' )", Conn);
                cmd1.Parameters.AddWithValue("@sa", chbSubAnalitika.Checked);
                cmd1.Parameters.AddWithValue("@po", chbPrenosOrgJed.Checked);
                cmd1.Parameters.AddWithValue("@s", chbSintetika.Checked);
                cmd1.ExecuteNonQuery();
            }
            else
            {
                SqlCommand cmd1 = new SqlCommand("UPDATE FinKontniPlan SET NazivKonta=@NazivKonta, sintetika=@Sin, PrenosPoOrgJed=@PPOJ, SubAnalitika=@SA WHERE Konto=@Konto and idkorisnik=" + KorID, Conn);
                cmd1.Parameters.AddWithValue("@NazivKonta", txtOpis.Text.ToString());
                cmd1.Parameters.AddWithValue("@Sin", chbSintetika.Checked);
                cmd1.Parameters.AddWithValue("@PPOJ", chbPrenosOrgJed.Checked);
                cmd1.Parameters.AddWithValue("@SA", chbSubAnalitika.Checked);
                cmd1.Parameters.AddWithValue("@Konto", txtAnaKonto.Text.ToString());
                cmd1.ExecuteNonQuery();
            }
        }

        protected void txtAnaKonto_TextChanged(object sender, EventArgs e)
        {
            Conn.Open();
            var Firma = Session["firma"];
            var Godina = Session["Godina"];
            var KorID = Session["Username"];
            SqlDataAdapter cmd = new SqlDataAdapter("select * from FinKontniPlan where idfirma=" + Firma + " and konto like '" + txtAnaKonto.Text + "'", Conn);
            DataTable ss = new DataTable();
            cmd.Fill(ss);

            if (ss.Rows.Count == 0)
            {
                btnSpasi.Text = "Spremi";
                chbSintetika.Focus();
            }
            else
            {
                txtOpis.Text = ss.Rows[0][3].ToString();
                chbSintetika.Checked = Convert.ToBoolean(ss.Rows[0][8]);
                chbSubAnalitika.Checked = Convert.ToBoolean(ss.Rows[0][4]);
                chbPrenosOrgJed.Checked = Convert.ToBoolean(ss.Rows[0][7]);
                btnSpasi.Text="Izmjeni";
                chbSintetika.Focus();
            }

        }

    }
}