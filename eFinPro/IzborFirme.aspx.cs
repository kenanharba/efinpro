﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace eFinProWeb
{
    public partial class IzborFirme : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            if (!IsPostBack)
            {
                lblSessionUsername.Text += Session["username"];
                if ((lblSessionUsername.Text == "") || (string.IsNullOrEmpty(lblSessionUsername.Text)))
                {
                    Response.Write("<script>alert('Sesija istekla');</script>");
                    Response.Redirect("login.aspx");
                }
                else
                {
                    lblSessionUsername.Text +=  Session["username"];
                    cbxFirma.Focus();
                }
            }
        }

        protected void btnNovaFirma_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            var KorID = Session["KorID"];
            SqlDataAdapter cmd = new SqlDataAdapter("select brlicenci from tlbKorisnik where korisnikID=" + KorID, Conn);
            DataTable ds = new DataTable();
            cmd.Fill(ds);
            SqlCommand cmd1 = new SqlCommand("select COUNT(*) from tlbMatPodatci INNER JOIN tlbPeriod ON tlbMatPodatci.IDFirma = tlbPeriod.IDFirma WHERE tlbMatPodatci.KorisnikID=" + KorID, Conn);
            int count = Convert.ToInt32(cmd1.ExecuteScalar());
            int a = Convert.ToInt32(ds.Rows[0][0]);
            if (count < a)
            {
                Response.Redirect("MaticniPodatciOFirmi.aspx");
            }
            else
            {
                Response.Write("<script>alert('Broj plačenih licenci iznosi " + a + " broj aktivnih Perioda je " + count + ".');</script>");
            }
        }

        protected void btnNoviPeriod_Click(object sender, EventArgs e)
        {
            Response.Redirect("PregledPerioda.aspx");
        }

        protected void btnRegFirm_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["MydbConnString"].ConnectionString);
            Conn.Open();
            int i, b;
            i = 1;
            b = 24;
            Session["Godina"] = cbxGodina.Text;
            Session["Firma"] = cbxFirma.Text;

            var KorID = Session["KorID"];
            var Firma = Session["firma"];
            var Godina = Session["Godina"];
            Response.Redirect("FKKnjinjenjeNaloga.aspx");
            
        }

        protected void cbxFirma_TextChanged(object sender, EventArgs e)
        {
            cbxGodina.Focus();
        }

    }
}