﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Glavna.Master" AutoEventWireup="true" CodeBehind="OdgovornaOsoba.aspx.cs" Inherits="eBilansWeb.OdgovornaOsoba" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblSessionUsername" runat="server" Text=" Aktivan korisnik : "></asp:Label>
    <div>
        <br />
        <p>
            <asp:Label ID="Label3" runat="server" Text="Firma / Društvo"></asp:Label>
            <asp:DropDownList ID="cbxFirma" runat="server" DataSourceID="SqlDataSource1" DataTextField="PuniNazivDrustva" DataValueField="IDFirma" Style="text-align: left; margin-left: 26px"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDFirma], [PuniNazivDrustva] FROM [tlbMatPodatci] WHERE ([KorisnikID] = @KorisnikID)">
                <SelectParameters>
                    <asp:SessionParameter Name="KorisnikID" SessionField="korID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </p>
        <p>
            <asp:Label Text="Ime i prezime" runat="server" />
            <asp:TextBox ID="txtImeIPrezime" runat="server" Style="text-align: left; margin-left: 40px" Width="200px"></asp:TextBox>
        </p>
        <p>
            <asp:Label Text="Adresa" runat="server" />
            <asp:TextBox ID="txtAdresa" runat="server" Style="text-align: left; margin-left: 90px" Width="450px"></asp:TextBox>
        </p>
        <p>
            <asp:Label Text="JMBG" runat="server" />
            <asp:TextBox ID="txtJMBG" class="jmbg" runat="server" Style="text-align: left; margin-left: 100px" Width="150px"></asp:TextBox>
        </p>
        <p>
            <asp:Label Text="Funkcija" runat="server" />
            <asp:TextBox ID="txtFunkcija" runat="server" Style="text-align: left; margin-left: 80px" Width="150px"></asp:TextBox>
        </p>
        <p>
            <asp:Label Text="eMail" runat="server" />
            <asp:TextBox ID="txtEmail" runat="server" Style="text-align: left; margin-left: 100px" Width="450px"></asp:TextBox>
        </p>
        <script src="Scripts/jquery.js" type="text/javascript"></script>
        <script src="Scripts/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            //$('.BrDozv').mask('9999' + "/" + '9');
            $('.BrDozv').mask('99999');
            $('.jmbg').mask('9999999999999');
            $('.DatumDo').mask('99.99.9999');
        </script>
        <br />
        <asp:Button ID="btnSpasi" runat="server" OnClick="btnSpasi_Click" Text="Spasi" Style="margin-left: 140px" Width="100px" />
    </div>
</asp:Content>
