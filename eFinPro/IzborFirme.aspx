﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Glavna.Master" AutoEventWireup="true" CodeBehind="IzborFirme.aspx.cs" Inherits="eFinProWeb.IzborFirme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .auto-style5 {
            margin-left: 400px;
            margin-top: 150px;
            margin-bottom: 140px;
            overflow: hidden;
        }
    </style>
    <asp:Label ID="lblSessionUsername" runat="server" Text="" Visible="false"></asp:Label>
    <div class="auto-style5">
        <br />
        <tr>
            <td>
                <br />
                <asp:Label runat="server" Text="Firma / Društvo"></asp:Label>
                <br />
                <asp:DropDownList ID="cbxFirma" runat="server" DataSourceID="SqlDataSource1" DataTextField="PuniNazivFirme" DataValueField="IDFirma" AutoPostBack="True" OnTextChanged="cbxFirma_TextChanged">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDFirma], [PuniNazivFirme] FROM [ZFMP] WHERE ([IDKorisnik] = @KorisnikID)">
                    <SelectParameters>
                        <asp:SessionParameter Name="KorisnikID" SessionField="KorID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <br />
        <br />
        <asp:Label runat="server" Text="Godina"></asp:Label>
        <br />
        <asp:DropDownList ID="cbxGodina" runat="server" DataSourceID="SqlDataSource2" DataTextField="IDPeriod" DataValueField="IDPeriod" AutoPostBack="True">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MydbConnString %>" ProviderName="<%$ ConnectionStrings:MydbConnString.ProviderName %>" SelectCommand="SELECT [IDPeriod] FROM [ZFPeriod] WHERE ([IDFirma] = @IDFirma)">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbxFirma" Name="IDFirma" PropertyName="SelectedValue" Type="Int32" />
<%--                <asp:ControlParameter ControlID="cbxObrPeriod" Name="ObrPeriod" PropertyName="SelectedValue" Type="Int32" />--%>
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <%--        <br />
        <br />
        <br />--%>
        <asp:Button ID="btnRegFirm" runat="server" Text="Izbor Firme" Width="180px" Height="45px" OnClick="btnRegFirm_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnNovaFirma" runat="server" Text="Unos Nove Firme" Width="180px" Height="45px" OnClick="btnNovaFirma_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnNoviPeriod" runat="server" Text="Unos Novog Perioda" Width="180px" Height="45px" OnClick="btnNoviPeriod_Click" />
        <br />
        <br />
        <br />
    </div>
</asp:Content>
