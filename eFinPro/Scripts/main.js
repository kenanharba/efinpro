﻿var Main = {

    switchSubMenu: function (subMenu) {
        if ($('#' + subMenu).css('display') == 'none') {
            Main.showSubMenu(subMenu);
        } else {
            Main.hideSubMenu(subMenu);
        }
    },

    showSubMenu: function (subMenu) {
        $("#" + subMenu).css({ "display": "contents", "opacity": "1" });
        $("#" + subMenu + "Arrow").html('&#9652;');
    },

    hideSubMenu: function (subMenu) {
        $("#" + subMenu).css({ "display": "none", "opacity": "0" });
        $("#" + subMenu + "Arrow").html('&#9662;');
    }
};

//var VrOrg = {

//    switchMenu: function (Menu) {
//        if ($('#' + Menu).css('display') == 'none') {
//            VrOrg.showMenu(Menu);
//        } else {
//            VrOrg.hideMenu(Menu);
//        }
//    },

//    showMenu: function (Menu) {
//        $("#" + Menu).css({ "display": "contents", "opacity": "1" });
//        //$("#" + subMenu + "Arrow").html('&#9652;');
//    },

//    hideMenu: function (Menu) {
//        $("#" + Menu).css({ "display": "none", "opacity": "0" });
//        //$("#" + subMenu + "Arrow").html('&#9662;');
//    }

//}