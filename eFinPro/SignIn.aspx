﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Glavna.Master" AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="eFinProWeb.SignIn" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <br />
    <br />
        <br />
    <br />
    <br />
    <br />--%>
<div style="margin-left: 450px; margin-top:200px; margin-bottom:150px">
    <tr>
        <td><asp:Label id="lblNazivKorisnika" runat="server" Text="Naziv Korisnika" Width="150px"></asp:Label></td>
        <td><asp:TextBox ID="txtNazivKorisnika" runat="server" Width="300px"></asp:TextBox></td>
    </tr>   
    <br />
    <tr>
        <td><asp:Label id="lblEmail" runat="server" Text="e-mail Korisnika" Width="150px"></asp:Label></td>
        <td><asp:TextBox ID="txtEmail" runat="server" Width="300px"></asp:TextBox></td>
    </tr>  
    <br />
    <tr>
        <td><asp:Label id="lblUsername" runat="server" Text="UserName" Width="150px"></asp:Label></td>
        <td><asp:TextBox ID="txtUsername" runat="server" Width="150px"></asp:TextBox></td>
    </tr>
    <br />
    <tr>
        <td><asp:Label id="lblPassword" runat="server" Text="Password" Width="150px"></asp:Label></td>
        <td><asp:TextBox ID="txtPassword" runat="server" Width="150px" TextMode="Password"></asp:TextBox></td>
    </tr>
    <br />
    <tr>
        <td><asp:Label id="lblPonoviPassword" runat="server" Text="Ponovi Password" Width="150px"></asp:Label></td>
        <td><asp:TextBox ID="txtPassPonovo" runat="server" Width="150px" TextMode="Password"></asp:TextBox></td>
    </tr>

    <br />
    <br />
    <br />
    <asp:Button ID="Spasi" runat="server" Text="Spasi" OnClick="Spasi_Click" Font-Bold="False" Font-Italic="False" Font-Size="Large" Width="180px" Height="45px" />
</div>

</asp:Content>
